FROM golang:1.13 AS build
WORKDIR /go/src/gitlab.com/gitops-demo/apps/my-go-app5
COPY . .
RUN CGO_ENABLED=0 go build -i -o ./my-app
 
FROM alpine:latest AS release
COPY --from=build /go/src/gitlab.com/gitops-demo/apps/my-go-app5/my-app /usr/local/bin/my-app
EXPOSE 3000
ENTRYPOINT ["/usr/local/bin/my-app"]